import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    getBack: function() {
      this.transitionToRoute('users');
    }
  }
});
