/* jshint ignore:start */

define('mg-fs-analyzer/config/environment', ['ember'], function(Ember) {
  var prefix = 'mg-fs-analyzer';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = Ember['default'].$('meta[name="' + metaName + '"]').attr('content');
  var config = JSON.parse(unescape(rawConfig));

  return { 'default': config };
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

if (runningTests) {
  require("mg-fs-analyzer/tests/test-helper");
} else {
  require("mg-fs-analyzer/app")["default"].create({"name":"mg-fs-analyzer","version":"0.0.0.3c3e1cd1"});
}

/* jshint ignore:end */
