define('mg-fs-analyzer/controllers/password', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  exports['default'] = Ember['default'].Controller.extend({
    actions: {
      getBack: function getBack() {
        this.transitionToRoute('index');
      }
    }
  });

});