define('mg-fs-analyzer/controllers/application', ['exports', 'ember', 'jquery'], function (exports, Ember, $) {

  'use strict';

  exports['default'] = Ember['default'].Controller.extend({
    menus: [{
      //href: "#",
      childMenus: 'true',
      collapse: 'collapseOne',
      icons: 'fa fa-desktop',
      text: '监控中心',
      items: [{
        href: 'fs-events',
        icons: 'fa fa-file',
        text: '文件操作' }, {
        href: 'sensors',
        icons: 'fa fa-wrench',
        text: '安全代理' }]
    }, {
      //href: "#",
      collapse: 'collapsefive',
      icons: 'fa fa-users',
      text: '用户管理',
      childMenus: 'true',
      items: [{
        href: 'users',
        icons: 'fa fa-user',
        text: '用户管理' }, {
        href: 'account',
        icons: 'fa fa-wrench',
        text: '个人信息' }, {
        href: 'password',
        icons: 'fa fa-key',
        text: '修改密码' }]
    }],

    signedIn: (function () {
      return !Ember['default'].isNone(this.get('userToken'));
      //return !(token === undefined || token === "" || token ===null);
    }).property('userToken'),

    userToken: (function (key, value) {
      return sessionStorage.token;
    }).property(),

    actions: {
      login: function login() {
        var _this = this;
        var username = $['default']('#username').val();
        var password = $['default']('#password').val();
        if (username.replace(/(^\s*)|(\s*$)/g, '') === '') {
          alert('用户名不能为空');
          return;
        }
        if (password.replace(/(^\s*)|(\s*$)/g, '') === '') {
          alert('密码不能为空');
          return;
        }
        $['default'].ajax({
          url: '',
          data: JSON.stringify({
            user: {
              username: username,
              password: password }
          }),
          contentType: 'appliction/json; charset=utf-8',
          type: 'POST',
          success: function success(data) {
            sessionStorage.setItem('token', data.token);
            sessionStorage.setItem('username', data.username);
            sessionStorage.setItem('role', data.role);
            sessionStorage.setItem('access_locked', data.access_locked);
            //_this.transitionToRoute('index');

            // FIXME
            _this.set('userToken', '');
            location.reload();
          },
          error: function error(jqXHP, status, errorThrown) {
            //alert('用户名或密码错误！');
            sessionStorage.setItem('token', 'data.token');
            _this.transitionToRoute('index');
            location.reload();
          }
        });
      },

      logout: function logout() {
        sessionStorage.clear();
        this.transitionToRoute('login');
        location.reload();
      }
    }
  });

});