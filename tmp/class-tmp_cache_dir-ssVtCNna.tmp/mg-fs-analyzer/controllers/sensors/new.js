define('mg-fs-analyzer/controllers/sensors/new', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  exports['default'] = Ember['default'].Controller.extend({
    actions: {
      getBack: function getBack() {
        this.transitionToRoute('users');
      }
    }
  });

});