define('mg-fs-analyzer/controllers/users', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  exports['default'] = Ember['default'].Controller.extend({
    //set table title and bind attr
    tableContent: [{
      thead: [{
        text: '#'
      }, {
        text: '用户名'
      }, {
        text: '真实姓名'
      }, {
        text: '电子邮箱'
      }, {
        text: '角色'
      }, {
        text: '是否锁定'
      }, {
        text: 'operation'
      }],
      tbody: [{
        contentText: 'First Name'
      }, {
        contentText: 'Last Name'
      }, {
        contentText: 'username@email'
      }]
    }]
  });

});