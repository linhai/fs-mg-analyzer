define('mg-fs-analyzer/router', ['exports', 'ember', 'mg-fs-analyzer/config/environment'], function (exports, Ember, config) {

  'use strict';

  var Router = Ember['default'].Router.extend({
    location: config['default'].locationType
  });

  Router.map(function () {
    this.route('login');
    this.route('password');
    this.route('account');
    this.route('users', { path: '/users' }, function () {
      this.route('new');
      //this.route('edit', { path: '/user/:user_id'});
      this.route('user', { path: '/user' });
    });

    this.route('fs-events', { path: '/fs-events' }, function () {
      this.route('new');
      this.route('fs-event', { path: '/fs-event' });
      this.route('fs-statistics', { path: '/fs-statistics' });
    });

    this.route('sensors', { path: '/sensors' }, function () {
      this.route('new');
      this.route('sensor', { path: '/sensor' });
    });
  });

  exports['default'] = Router;

});