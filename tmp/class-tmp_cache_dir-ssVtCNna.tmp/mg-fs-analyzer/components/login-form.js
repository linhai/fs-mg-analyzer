define('mg-fs-analyzer/components/login-form', ['exports', 'ember'], function (exports, Ember) {

  'use strict';

  exports['default'] = Ember['default'].Component.extend({
    actions: {
      login: function login() {
        this.sendAction();
      }
    }
  });

});