define('mg-fs-analyzer/tests/test-helper', ['mg-fs-analyzer/tests/helpers/resolver', 'ember-qunit'], function (resolver, ember_qunit) {

	'use strict';

	ember_qunit.setResolver(resolver['default']);

});