define('mg-fs-analyzer/tests/controllers/application.jshint', function () {

  'use strict';

  module('JSHint - controllers');
  test('controllers/application.js should pass jshint', function() { 
    ok(false, 'controllers/application.js should pass jshint.\ncontrollers/application.js: line 49, col 29, \'value\' is defined but never used.\ncontrollers/application.js: line 49, col 24, \'key\' is defined but never used.\ncontrollers/application.js: line 87, col 38, \'errorThrown\' is defined but never used.\ncontrollers/application.js: line 87, col 30, \'status\' is defined but never used.\ncontrollers/application.js: line 87, col 23, \'jqXHP\' is defined but never used.\n\n5 errors'); 
  });

});