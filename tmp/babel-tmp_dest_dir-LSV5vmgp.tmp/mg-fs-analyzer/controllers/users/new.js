import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    getBack: function getBack() {
      this.transitionToRoute('users');
    }
  }
});