import Ember from 'ember';

export default Ember.Controller.extend({
  //set table title and bind attr
  tableContent: [{
    thead: [{
      text: '#'
    }, {
      text: '用户名'
    }, {
      text: '真实姓名'
    }, {
      text: '电子邮箱'
    }, {
      text: '角色'
    }, {
      text: '是否锁定'
    }, {
      text: 'operation'
    }],
    tbody: [{
      contentText: 'First Name'
    }, {
      contentText: 'Last Name'
    }, {
      contentText: 'username@email'
    }]
  }]
});