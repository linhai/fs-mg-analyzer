import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function () {
  this.route('login');
  this.route('password');
  this.route('account');
  this.route('users', { path: '/users' }, function () {
    this.route('new');
    //this.route('edit', { path: '/user/:user_id'});
    this.route('user', { path: '/user' });
  });

  this.route('fs-events', { path: '/fs-events' }, function () {
    this.route('new');
    this.route('fs-event', { path: '/fs-event' });
    this.route('fs-statistics', { path: '/fs-statistics' });
  });

  this.route('sensors', { path: '/sensors' }, function () {
    this.route('new');
    this.route('sensor', { path: '/sensor' });
  });
});

export default Router;